<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\User\IndexResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\User;

class AuthController extends Controller
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function register(RegisterRequest $request): JsonResponse
    {
        if (Auth::check()) {
            return response()->json([
                'success' => 'false',
                'message' => 'You are authenticated'
            ]);
        }

        User::create($request->validated());
        return response()->json([
            'success' => 'true',
            'message' => 'User created'
        ]);
    }

    public function login (LoginRequest $request): JsonResponse
    {
        $checkByLogin = $this->repository->checkUserByLogin($request->get('login'));
        if (!$checkByLogin) {
            return response()->json([
                'success' => false,
                'errors'  => [
                    'login' => 'User not found'
                ]
            ], 404);
        }

        if (!Auth::attempt($request->validated())) {
            return response()->json([
                'success' => true,
                'errors'  => [
                    'password' => 'Your data does not match'
                ]
            ], 400);
        };

        if (Auth::attempt($request->validated())) {
            $token = Auth::user()->createToken('api-token', ['user']);
            return response()->json([
                'success' => true,
                'token' => $token->plainTextToken,
                'user' => new IndexResource(auth()->user()),
            ]);
        };

    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response()->json([
            'success' => true,
            'message' => 'You are out of the system'
        ]);
    }

}
