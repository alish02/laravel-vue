<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use JetBrains\PhpStorm\ArrayShape;

class CategoryController extends Controller
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(): JsonResponse
    {
        return response()->json($this->repository->getCategories());
    }

    public function store(StoreRequest $request): JsonResponse
    {
        $response = $this->repository->store($request->validated());

        return response()->json([
            'success' => true,
            'data' => $this->show($response)
        ], 201);
    }

    public function update(UpdateRequest $request, Category $category): JsonResponse
    {
        $validated = $request->validated();
        $this->repository->update($validated, $category);

        return response()->json([
           'success' => true,
           'data' => $this->show($category),
        ]);
    }

    public function show(Category $category): array
    {
        return [
            'name'        => $category['name'],
            'description' => $category['description'],
            'picture'     => $category['picture'],
            'is_active'   => $category['is_active'],
            'user'        => $category->user()
        ];
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return response()->json(['success' => true]);
    }
}
