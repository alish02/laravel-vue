<?php

namespace App\Http\Controllers;

use App\Http\Resources\User\IndexResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index (): JsonResponse
    {
        return response()->json(new IndexResource(auth()->user()));
    }

    public function getUsers(): JsonResponse
    {
        return response()->json($this->repository->getUsers());
    }
}
