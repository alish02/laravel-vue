<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;

class LoginRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'login'    => ['required'],
            'password' => ['required']
        ];
    }
}
