<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;
use App\Models\User;

class RegisterRequest extends ApiRequest
{

    public function rules()
    {
        return [
            'name'     => ['required'],
            'surname'  => ['required'],
            'email'    => ['required', 'email', 'unique:users'],
            'login'    => ['required', 'unique:users'],
            'password' => ['required']
        ];
    }
}
