<?php

namespace App\Http\Requests\Category;

use App\Http\Requests\ApiRequest;
use App\Models\Category;

class StoreRequest extends ApiRequest
{

    public function rules()
    {
        return [
            'name'        => ['required', 'string', 'unique:categories'],
            'description' => ['required', 'string'],
            'picture'     => ['required', 'mimes:jpeg,jpg,svg,png'],
            'is_active'   => ['required', 'boolean']
        ];
    }
}
