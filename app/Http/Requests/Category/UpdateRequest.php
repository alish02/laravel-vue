<?php

namespace App\Http\Requests\Category;

use App\Http\Requests\ApiRequest;

class UpdateRequest extends ApiRequest
{

    public function rules()
    {
        return [
            'name'        => ['string'],
            'description' => ['string'],
            'picture'     => ['mimes:jpeg,jpg,svg,png'],
            'is_active'   => ['boolean']
        ];
    }
}
