<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Category\IndexResource as CategoryListResource;

class IndexResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'surname' => $this->surname,
            'login' => $this->login,
            'email' => $this->email,
            'categories' => CategoryListResource::collection($this->categories)
        ];
    }
}
