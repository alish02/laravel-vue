<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Category extends Model
{
    use HasFactory;
    protected $guarded = false;

    protected $casts = [
        'is_active' => 'boolean'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)
            ->select(['id', 'name', 'surname', 'login', 'email']);
    }

    protected function isActive(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value,
            set: fn($value) => ucfirst($value)
        );
    }
}
