<?php

namespace App\Repositories;

use App\Http\Resources\Category\CategoryListResource;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CategoryRepository
{

    public function getCategories()
    {
        return CategoryListResource::collection(Category::all()->sortByDesc('id'));
    }

    public function store(array $data)
    {
        if (is_file($data['picture']))
            $picturePath = Storage::putFile('admin/category', $data['picture']);

        return Category::create([
            'name'        => $data['name'],
            'description' => $data['description'],
            'picture'     => $picturePath ?? NULL,
            'is_active'   => $data['is_active'],
            'user_id'     => Auth::user()->getAuthIdentifier()
        ]);
    }

    public function update(array $data, object $category)
    {
        if (isset($data['picture']) && is_file($data['picture']))
            $picturePath = Storage::putFile('admin/category', $data['picture']);

        return $category->update([
            'name'        => $data['name'] ?? $category['name'],
            'description' => $data['description'] ?? $category['description'],
            'picture'     => $picturePath ?? $category['picture'],
            'is_active'   => $data['is_active'] ?? $category['is_active'],
            'user_id'     => Auth::user()->getAuthIdentifier()
        ]);
    }
}
