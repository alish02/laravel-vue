<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function checkUserByLogin(string $login): bool
    {
        return User::query()->where('login', $login)->exists();
    }

    public function getUsers()
    {
        return User::query()->select(['id', 'name', 'surname'])->get();
    }
}
