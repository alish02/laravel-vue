"use strict";

import Vue from 'vue';
import axios from "axios";
import store from "@/store"
import Cookies from "js-cookie"

let config = {
  // baseURL: process.env.baseURL || process.env.apiUrl || ""
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
};

const _axios = axios.create(config);

_axios.defaults.withCredentials = true
_axios.defaults.baseURL = process.env.VUE_APP_API_URL
_axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
_axios.defaults.headers['Accept'] = 'application/json'
_axios.defaults.headers['Content-Type'] = 'application/json'
_axios.interceptors.request.use(request => {
    const token = store.getters['user/token']
    if (token) {
        request.headers.common.Authorization = `Bearer ${token}`
    }
    if (['post', 'put', 'delete'].includes(request.method) && !Cookies.get('XSRF-TOKEN')) {
        return setCSRFToken().then(() => request)
    }

    return request
})

_axios.interceptors.request.use(
  function(config) {
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

_axios.interceptors.response.use(
  function(response) {
    return response;
  },
  function(error) {
    return Promise.reject(error);
  }
);

const setCSRFToken = () => {
    return _axios.get('sanctum/csrf-cookie');
}

Plugin.install = function(Vue, options) {
  Vue.axios = _axios;
  window.axios = _axios;
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return _axios;
      }
    },
    $axios: {
      get() {
        return _axios;
      }
    },
  });
};

Vue.use(Plugin)

export default Plugin;
