import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthRegister from '@/pages/auth/Register';
import AuthLogin from '@/pages/auth/Login';
import AdminIndex from '@/pages/admin/Index';
import CategoryIndex from "@/pages/admin/category/Index";
import CategoryCreate from "@/pages/admin/category/Create";
import CategoryUpdate from "@/pages/admin/category/Update";

Vue.use(VueRouter)

const routes = [
    {
        path: '/register',
        name: 'AuthRegister',
        component: AuthRegister,
        meta: {
            layout: 'AuthLayout'
        }
    },
    {
        path: '/login',
        name: 'AuthLogin',
        component: AuthLogin,
        meta: {
            layout: 'AuthLayout'
        }
    },
    {
        path: '/admin',
        name: AdminIndex,
        component: AdminIndex,
        meta: {
            layout: 'AdminLayout'
        },
    },
    {
        path: '/admin/categories',
        name: CategoryIndex,
        component: CategoryIndex,
        meta: {
            layout: 'AdminLayout'
        },
    },
    {
        path: '/admin/categories/create',
        name: CategoryCreate,
        component: CategoryCreate,
        meta: {
            layout: 'AdminLayout'
        },
    },
    {
        path: '/admin/categories/:id',
        name: CategoryUpdate,
        component: CategoryUpdate,
        meta: {
            layout: 'AdminLayout'
        },
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
