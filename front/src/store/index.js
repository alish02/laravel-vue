import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";
import user from "@/store/modules/user";
import category from "@/store/modules/category";

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        user,
        category
    },
    plugins: [createPersistedState()]
})
