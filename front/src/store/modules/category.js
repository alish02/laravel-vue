import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex)

export default {
    namespaced: true,
    state: {
        categories: {},
    },
    getters: {
        categories: state => state.categories
    },
    mutations: {
        SET_CATEGORIES: (state, data) => {
            state.categories = data
        }
    },
    actions: {
        getCategories({commit}) {
            axios.get('api/categories')
                .then(response => {
                    commit('SET_CATEGORIES', response.data)
                })
        },
        destroy({commit}, id) {
            axios.delete('api/categories/'+id)
                .then(response => {
                    console.log(response.data)
                    location.reload()
                })
        }
    }
}
