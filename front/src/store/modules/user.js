import Vue from 'vue'
import Vuex from 'vuex'
import Cookies from "js-cookie";

Vue.use(Vuex)

export default {
    namespaced: true,
    state: {
        user: {},
        token: {},
        errors: {}
    },
    getters: {
        user: state => state.user,
        token: state => state.token,
        errors: state => state.errors,
    },
    mutations: {
        SET_USER: (state, data) => {
            state.user = data;
        },
        SET_TOKEN: (state, data) => {
            state.token = data
        },
        SET_ERRORS: (state, data) => {
            state.errors = data
        }
    },
    actions: {
        clear({commit}) {
            commit('SET_USER', {})
            commit('SET_TOKEN', {})
            Cookies.remove('token')
        },
        setToken({commit}, data) {
            commit('SET_TOKEN', data)
            if (data) {
                Cookies.set('token', data, {expires: (1 / 1440) * 20})
            }
        },
        register({commit}, data) {
            axios.post('api/register', {
                name: data.name,
                surname: data.surname,
                login: data.login,
                email: data.email,
                password: data.password
            }).then(response => {
                commit('SET_ERRORS', {})
                window.location.href = '/login'
            }).catch(errors => {
                let data = {}
                for (const [key, value] of Object.entries(errors.response.data.errors)) {
                    data[key] = value[0]
                }
                if (Object.keys(data).length > 0) {
                    commit('SET_ERRORS', data)
                }
            })
        },
        login({commit}, data) {
            axios.post('api/login', {
                login: data.login,
                password: data.password
            }).then(response => {
                this.dispatch('user/setToken', response.data.token)
                commit('SET_ERRORS', {})
                commit('SET_USER', response.data.user)
                window.location.href = '/admin'
            }).catch(errors => {
                let data = {}
                for (const [key, value] of Object.entries(errors.response.data.errors)) {
                    data[key] = value
                }
                if (Object.keys(data).length > 0) {
                    commit('SET_ERRORS', data)
                }
                this.dispatch('user/clear')
            })
        },
        logout() {
            axios.post('api/logout').then(response => {
                this.dispatch('user/clear')
                window.location.href = '/login'
            })
        }
    }
}
