import {createRouter, createWebHistory} from 'vue-router'
import Register from '@/pages/auth/Register';
import Login from '@/pages/auth/Login';
import AdminIndex from '@/pages/admin/Index';

const routes = [
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      layout: 'AuthLayout'
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      layout: 'AuthLayout'
    }
  },
  {
    path: '/admin',
    name: AdminIndex,
    component: AdminIndex,
    meta: {
      layout: 'AdminLayout'
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
